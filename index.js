//[SECTION] EXPONENT OPERATOR

// USING MATH OBJECT METHODS

const firstNum = Math.pow(8, 2);
console.log(firstNum);

//using the exponent OPERATOR
const secondNum = 8 ** 2;
console.log(secondNum);


//[Section] Template Literals

let name = "John";

//Hello John! Welcome to Programming!
//Pre-Template Literals

let message = "Hello " + name + "! Welcome to programming!";
console.log("Message without template literals: ");
console.log(message);

//strings using template literals = ``
//uses backticks (``) instead of ("") or single ('')

message = `Hello ${name}! Welcome to programming!`;
console.log("Message with template literals: ");
console.log(message);

const anotherMessage = `${name} attended a math competition.
He won it by solving the problem "8 ** 2" with the solution of ${8**2}`;

console.log(anotherMessage);

//[SECTION] Array Destructuring

// It allows uis to name array elements with variableNames instead of using the index numbers.

/*
    -Syntax:
        let/const [variableName1, variableName2, VariableName3] = arrayName;
*/


const fullName = ["Juan", "Dela", "Cruz"];

//Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

//Hello Juan Dela Cruz! It's nice to meet you.
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]} It's nice to meet you.`);

// Array Destructuring

const arr = [firstName, middleName, lastName] = fullName;

const str = arr.join(' '); //<- to replace commas to whitespace

console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${str} It's nice to meet you.`);


//[SECTION] Object Destructuring

const person = {
  givenName: "Jane",
  maidenName: "Delay",
  surName: "Cruz",
}


//Pre-object Destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.surName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.lastName}! It's good to see you again.`);


//object Destructuring
// const {givenName, maidenName, surName} = person;
// console.log(givenName);
// console.log(maidenName);
// console.log(surName);


//console.log(`Hello ${givenName} ${maidenName} ${lastName}! It's good to see you again.`);
//Object

function getFullName({
  givenName,
  maidenName,
  surName
}) {

  console.log(`Hello ${givenName} ${maidenName} ${surName}!`);

}

getFullName(person);

// [SECTION] Arrow functions

/*
  - Compact alternative syntax to traditional functions.
  - useful for code snippets where creating functions will not be reused in another portion of the code.
  -"Dry" (Don't Repeat yourself) Principle

  -This will only work with "function expression".


    Syntax:
      let/const variableName = () =>{
          /code block/statement
      }
      let/const variableName = (parameter) => {
        //code block/statement with parameter
      }
*/



// function hello(){
//   console.log(`Hello World!`);
// }

const hello = () => {
  console.log(`Hello World!`)
}
hello();


const students = ["John", "Jane", "Judy"];

//arrow functions usings array iteration METHODS
//Pre-Arrow Function

// students.forEach(function(student){
//   console.log(`${student} is a student.`);
// });

students.forEach((student) => {
  console.log(`${student} is a student.`);
})

//[SECTION] Implicit Return statement
// There are instances when you can omit the "return" statement.
// const add = (x, y) => {
//   return x + y;
// }

//Using implicit return


const add = (x, y) => x + y;


let total = add(1, 2);
console.log(total);

const numbers = [1, 2, 3, 4, 5];

let squaredValues = numbers.map((number) => number ** 2);

console.log(squaredValues);

// [SECTION] Default Function argument Value
//Provides a default argument value if none is provided when the function is invoked.
// const greet = (name) => `Good morning, ${name}`;
// console.log(greet()); // no argument provided will result to undefind

const greet = (name = "User") => `Good morning, ${name}`;
console.log(greet());
console.log(greet("John"));

//[SECTION] Class-Based object blueprint

// Allows creation /Instantiation of object using classes as blueprints.

/*

    -Syntax:
      Class className{
        constructor(ojectPropertyA, objectPropertyB){
          this.objectPropertyA = objectPropertyA;
          this.objectPropertyB = objectPropertyB;
      }
      //insert function outside our constructor
    }

*/

// function Car(brand, name, year) {
//   this.brand = brand;
//   this.name = name;
//   this.year = year;
// }


class Car {
  constructor(brand, name, year) {
    this.brand = brand;
    this.name = name;
    this.year = year;
  }
}

const myCar = new Car();
console.log(myCar);

//reassign value of each properties

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2020;

console.log(myCar);









//[End]
